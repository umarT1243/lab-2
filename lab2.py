product_names = ["Water", "Redbull", "Coke"]
product_prices = [1, 1.9, 1.5]
coins = [0.05, 0.1, 0.2, 0,5, 1, 2]

mode = 1
while True:
  if mode == 1:
    print("Select product:")
    for i, product in enumerate(product_names):
      print(f"{i+1}. {product}")

    user_input = input()
    if user_input == "x":
      break
    else:
      product_index = int(user_input) - 1
      selected_product = product_names[product_index]
      print(f"Selected product: {selected_product}")
      amount = product_prices[product_index]
      mode = 2

  elif mode == 2:
    print(f"Payment: {amount}")
    print("Insert coins:")
    for i, coin in enumerate(coins):
      print(f"{i}. £{coin}")

    user_input = input()
    if user_input == "x":
      break
    else:
      coin_index = int(user_input)
      inserted_coin = coins[coin_index]
      amount -= inserted_coin
      if amount < 0:
        change = abs(amount)
        print(f"Change: {change}")
        break
